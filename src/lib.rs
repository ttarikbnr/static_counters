
#[allow(unused)]
macro_rules! counters {
    ($($i:ident),+) => {
        pub use __counters::take_diff;

        mod __counters {

            use std::sync::atomic::{AtomicUsize, Ordering};
    
            #[allow(non_snake_case)]
            pub struct CountersAtomic {
                $(pub(crate) $i: AtomicUsize,)+
            }
            
            impl CountersAtomic  {
                const fn default() -> Self {
                    Self {
                        $($i: AtomicUsize::new(0),)+
                    }
                }
            }

            impl Clone for CountersAtomic {
                fn clone(&self) -> Self {
                    Self {
                        $($i: AtomicUsize::new(self.$i.load(Ordering::Acquire)),)+
                    }
                }
            }

            #[allow(non_snake_case)]
            pub struct Diff {
                $(pub $i: i128,)+
            }

            impl std::ops::Sub<&CountersAtomic> for &CountersAtomic {
                type Output = Diff;
                fn sub(self, rhs: &CountersAtomic) -> Self::Output {
                    Self::Output {
                        $($i: self.$i.load(Ordering::Acquire) as i128 - rhs.$i.load(Ordering::Acquire) as i128,)+
                    }
                }
            }


            pub static mut __CountersAtomic : CountersAtomic = CountersAtomic::default();

            static mut __CountersAtomicOld : CountersAtomic = CountersAtomic::default();

            pub fn take_diff() -> Diff {
                unsafe {
                    let diff = &__CountersAtomic - &__CountersAtomicOld;
                    __CountersAtomicOld = __CountersAtomic.clone();
                    diff
                }
            }
        }
    };
}

macro_rules! increase_counter {
    ($i:ident) => {
        unsafe {
            __counters::__CountersAtomic.$i.fetch_add(1, std::sync::atomic::Ordering::AcqRel)
        }
    }
}

macro_rules! decrease_counter {
    ($i:ident) => {
        unsafe {
            __counters::__CountersAtomic.$i.fetch_sub(1, std::sync::atomic::Ordering::AcqRel)
        }
    }
}

macro_rules! get_counter {
    ($i:ident) => {
        unsafe {
            __counters::__CountersAtomic.$i.load(std::sync::atomic::Ordering::Acquire)
        }
    }
}

#[test]
fn test1() {
    counters!(A, B, C);

    increase_counter!(B);
    increase_counter!(B);
    increase_counter!(B);
    increase_counter!(B);
    println!("{:?}", take_diff().B);
    increase_counter!(B);
    increase_counter!(B);
    println!("{:?}", take_diff().B);

    println!("{:?}", get_counter!(B));
    
}